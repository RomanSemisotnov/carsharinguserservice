package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import ru.tinkoff.carsharing.userservice.db.repositories.{BookingRepository, RideRepository, UserRepository}
import ru.tinkoff.carsharing.userservice.services.{BookingService, CarService, RideService, UserService}
import sttp.tapir.endpoint
import sttp.tapir._
import sttp.tapir.json.circe.jsonBody

import scala.concurrent.Future
import scala.util.{Failure, Success}
import sttp.tapir.generic.auto._
import io.circe.generic.auto._
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.http.services.CarHttpService

trait BaseEndpoint {
  private implicit val as = ActorSystem()

  import as.dispatcher

  val rideRepository = new RideRepository
  protected val carHttpService = new CarHttpService()
  protected val userService = new UserService(new UserRepository)
  private val bookingRepository = new BookingRepository
  protected val bookingService = new BookingService(bookingRepository)
  protected val rideService = new RideService(
    bookingRepository,
    rideRepository,
    carHttpService
  )
  protected val carService = new CarService(rideRepository, carHttpService, rideService)

  private val baseEndpointV1 =
    endpoint.in("api" / "v1")

  protected val endpointWithoutAuth = baseEndpointV1
  protected val endpointWithAuth =
    baseEndpointV1
      .in(header[Int]("X-AUTH-TOKEN"))
      .errorOut(jsonBody[HttpRequestError])
      .serverLogicForCurrent(userId =>
        userService.findById(userId)
          .unsafeToFuture
          .map {
            case Some(v) => Right(v)
            case None => Left(HttpRequestError("user with that session doesnt found"))
          }
      )

  def handleErrors[T](f: Future[T]): Future[Either[HttpRequestError, T]] =
    f.transform {
      case Success(v) => Success(Right(v))
      case Failure(e) => Success(Left(HttpRequestError(e.getMessage)))
    }

}
