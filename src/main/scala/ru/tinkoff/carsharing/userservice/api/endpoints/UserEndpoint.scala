package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import io.circe.generic.auto._
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.db.models.User
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._

import scala.concurrent.Future

case class CreateUserRequest(login: String, pass: String)

class UserEndpoint extends BaseEndpoint{
  implicit val as = ActorSystem()
  import as.dispatcher

  private val baseUserEndpoint =
    endpointWithoutAuth.in("user")

  private val get =
    baseUserEndpoint
      .get
      .in(path[Long])
      .out(jsonBody[User])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { id: Long =>
        userService.findById(id)
          .unsafeToFuture
          .map {
            case Some(v) => Right(v)
            case None => Left(HttpRequestError("not found"))
          }
      }

  private val create =
    baseUserEndpoint
      .post
      .in(jsonBody[CreateUserRequest])
      .out(jsonBody[User])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { request: CreateUserRequest =>
        userService.insert(request.login, request.pass)
          .unsafeToFuture
          .map(Right.apply)
          .recover {
            case e => Left(HttpRequestError(e.getMessage))
          }
      }

  val all = List(get, create)

}
