package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.db.models.{Booking, Ride}
import sttp.tapir.json.circe.jsonBody
import io.circe.generic.auto._
import sttp.tapir.generic.auto._

class OrderEndpoint extends BaseEndpoint {
  private implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseOrderEndpoint =
    endpointWithAuth.in("order")

  case class OrderEndpointResponse(carId: List[Ride])
  private val orders =
    baseOrderEndpoint
      .get
      .out(jsonBody[OrderEndpointResponse])
      .serverLogic { case (user, _) =>
        rideService.getRides(user.id)
          .unsafeToFuture()
          .map {v => Right.apply(OrderEndpointResponse(v))}
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }


  val all = List(orders)


}
