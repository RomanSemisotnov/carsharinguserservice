package ru.tinkoff.carsharing.userservice.api.errors

case class HttpRequestError(message: String) extends Exception(message)
