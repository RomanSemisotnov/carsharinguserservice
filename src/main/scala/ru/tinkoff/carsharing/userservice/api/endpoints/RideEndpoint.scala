package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import sttp.tapir._
import sttp.tapir.json.circe.jsonBody

import sttp.tapir.generic.auto._
import io.circe.generic.auto._
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.db.models.Ride

import java.util.UUID

class RideEndpoint extends BaseEndpoint {
  private implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseRideEndpoint =
    endpointWithAuth.in("car" / "ride")

  case class StartRideRequest(bookingId: UUID)
  private val startRideEndpoint =
    baseRideEndpoint
      .post
      .in("start")
      .in(jsonBody[StartRideRequest])
      .out(jsonBody[Ride])
      .serverLogic { case (user, request) =>
        rideService.startRideByBooking(request.bookingId, user.id)
          .map {Right.apply}
          .recover {
          case e => Left(HttpRequestError(e.getMessage))
        }
      }

  case class EndRideRequest(rideId: UUID)
  private val endRideEndpoint =
    baseRideEndpoint
      .post
      .in("end")
      .in(jsonBody[EndRideRequest])
      .out(jsonBody[Ride])
      .serverLogic { case (user, request) =>
        rideService.endRide(request.rideId, user.id)
          .map {Right.apply}
          .recover {
            case e => Left(HttpRequestError(e.getMessage))
          }
      }

  val all =
    List(startRideEndpoint, endRideEndpoint)

}
