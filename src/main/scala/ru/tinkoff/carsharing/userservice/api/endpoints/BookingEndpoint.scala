package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import io.circe.generic.auto._
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.db.models.Booking
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir._

import java.util.UUID

final class BookingEndpoint extends BaseEndpoint {
  implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseBookingEndpoint =
    endpointWithAuth.in("car" / "booking")

  case class BookingStartRequest(carId: Long)
  private val start =
    baseBookingEndpoint
      .post
      .in("start")
      .in(jsonBody[BookingStartRequest])
      .out(jsonBody[Booking])
      .serverLogic { case (user, request) =>
        bookingService.makeBooking(user.id, request.carId)
          .unsafeToFuture()
          .map {Right.apply}
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  case class BookingEndRequest(bookingId: UUID)
  private val end =
    baseBookingEndpoint
      .post
      .in("end")
      .in(jsonBody[BookingEndRequest])
      .out(jsonBody[Booking])
      .serverLogic { case (user, request) =>
        bookingService.closeBooking(user.id, request.bookingId)
          .unsafeToFuture()
          .map {Right.apply}
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  val all = List(start, end)

}
