package ru.tinkoff.carsharing.userservice.api.endpoints

import akka.actor.ActorSystem
import io.circe.generic.auto._
import ru.tinkoff.carsharing.userservice.api.errors.HttpRequestError
import ru.tinkoff.carsharing.userservice.http.models.{AvailableCarResponse, CarResponse}
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._

import java.util.UUID

class CarEndpoint extends BaseEndpoint {
  private implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseCarEndpoint =
    endpointWithAuth.in("car")

  private val getAvailable =
    baseCarEndpoint
      .get
      .in("available")
      .out(jsonBody[AvailableCarResponse])
      .serverLogic { case (_, _) =>
        carHttpService.getAvailable()
          .map {
            Right.apply
          }.recover {
            case e => Left(HttpRequestError(e.getMessage))
          }
      }

  case class StartingCarRequest(rideId : UUID)
  private val startCar =
    baseCarEndpoint
      .post
      .in("start")
      .in(jsonBody[StartingCarRequest])
      .out(jsonBody[CarResponse])
      .serverLogic { case (user, request) =>
        carService.start(request.rideId, user.id)
          .map {
            Right.apply
          }.recover {
          case e =>
            e.printStackTrace()
            Left(HttpRequestError(e.getMessage))
        }
      }

  case class EndCarRequest(rideId: UUID)
  private val endCar =
    baseCarEndpoint
      .post
      .in("end")
      .in(jsonBody[EndCarRequest])
      .out(jsonBody[CarResponse])
      .serverLogic { case (user, request) =>
        carService.end(request.rideId, user.id)
          .map {
            Right.apply
          }.recover {
          case e =>
            e.printStackTrace()
            Left(HttpRequestError(e.getMessage))
        }
      }

  val all = List(getAvailable, startCar, endCar)

}
