package ru.tinkoff.carsharing.userservice.services

import akka.actor.ActorSystem
import cats.effect.IO
import ru.tinkoff.carsharing.userservice.db.repositories.BookingRepository
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.exceptions.{BookingAlreadyClosedException, CarAlreadyUsedException, ModelNotFoundException}
import ru.tinkoff.carsharing.userservice.db.models.Booking
import ru.tinkoff.carsharing.userservice.http.services.CarHttpService

import java.util.UUID
import scala.concurrent.Future

class BookingService(
                      bookingRepository: BookingRepository
                    ) {
  import ru.tinkoff.carsharing.userservice.db.configs.DoobieConfig.conn
  implicit val as = ActorSystem()
  import as.dispatcher

  def makeBooking(userId: Long, carId: Long): IO[Booking] =
    (for {
      isAvailable <- bookingRepository.checkCarAvailable(carId)
      booking <- isAvailable match {
        case true => bookingRepository.makeBooking(userId, carId)
        case false => throw CarAlreadyUsedException(carId)
      }
    } yield booking)
      .transact(conn)

  def closeBooking(userId: Long, bookingId: UUID): IO[Booking] =
    (for {
      bookingOpt <- bookingRepository.findByUserIdAndBookingId(userId, bookingId)
      booking = bookingOpt
        .map { b =>
          b.ended match {
            case Some(_) => throw BookingAlreadyClosedException(bookingId)
            case None => b
          }}
        .getOrElse(throw ModelNotFoundException("Booking", (userId, bookingId).toString()))
      closed <- bookingRepository.closeBooking(booking.id, 20)
    } yield closed).transact(conn)

}
