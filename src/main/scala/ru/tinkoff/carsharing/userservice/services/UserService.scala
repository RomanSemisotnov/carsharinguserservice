package ru.tinkoff.carsharing.userservice.services

import cats.effect.IO
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.exceptions.ModelAlreadyExistException
import ru.tinkoff.carsharing.userservice.db.models.User
import ru.tinkoff.carsharing.userservice.db.repositories.UserRepository


class UserService(userRepository: UserRepository) {

  import ru.tinkoff.carsharing.userservice.db.configs.DoobieConfig.conn

  def findById(id: Long): IO[Option[User]] =
    userRepository
      .findById(id)
      .transact(conn)

  def insert(login: String, pass: String): IO[User] =
    (for {
      existedUser <- userRepository.findByLogin(login)
      _ <- existedUser match {
        case Some(_) => throw ModelAlreadyExistException("User", login)
        case None => userRepository.insert(login, pass)
      }
      id <- userRepository.getLastVal()
      createdUser <- userRepository.findById(id)
    } yield createdUser).transact(conn).map(_.get)

}
