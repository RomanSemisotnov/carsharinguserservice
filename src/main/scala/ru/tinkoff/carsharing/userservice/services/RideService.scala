package ru.tinkoff.carsharing.userservice.services

import akka.actor.ActorSystem
import ru.tinkoff.carsharing.userservice.db.exceptions.{BookingAlreadyClosedException, ModelNotFoundException, RideAlreadyEndedException, RideAlreadyStartedException}
import ru.tinkoff.carsharing.userservice.db.repositories.{BookingRepository, RideRepository}
import ru.tinkoff.carsharing.userservice.http.services.CarHttpService
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.models.Ride

import java.util.UUID
import scala.concurrent.Future

class RideService(
                 bookingRepository: BookingRepository,
                 rideRepository: RideRepository,
                 carHttpService: CarHttpService
                 ) {
  import ru.tinkoff.carsharing.userservice.db.configs.DoobieConfig.conn
  implicit val as = ActorSystem()
  import as.dispatcher

  def getRides(userId: Long) =
    rideRepository.getRides(userId)
      .transact(conn)

  def getActiveRide(rideId: UUID, userId: Long) =
    for {
      rideOpt <- rideRepository.findByRideIdAndUserId(rideId, userId)
      ride = rideOpt.map { r =>
        r.ended match {
          case Some(_) => throw RideAlreadyEndedException(rideId)
          case None => r
        }
      }.getOrElse(throw ModelNotFoundException("Ride", (rideId, userId)))
    } yield ride

  def startRideByBooking(bookingId: UUID, userId: Long) = {
    val notClosedBookingStatement = (for {
      bookingOpt <- bookingRepository.findByUserIdAndBookingId(userId, bookingId)
      notClosedBooking = bookingOpt.map { b =>
        b.ended match {
          case Some(_) => throw BookingAlreadyClosedException(bookingId)
          case None => b
        }
      }.getOrElse(throw ModelNotFoundException("Booking", bookingId))
      rideOpt <- rideRepository.findByByBookingAndUserIdNotEnded(bookingId, userId)
      _ = rideOpt.map{
        case _ => throw RideAlreadyStartedException(s"by bookingId: $bookingId")
      }
    } yield notClosedBooking).transact(conn).unsafeToFuture()

    for {
      notClosedBooking <- notClosedBookingStatement
      newRidingInCarServiceResponse <- carHttpService.startRide(notClosedBooking.carId)
      newRideInOurService <- rideRepository.startRide(userId, notClosedBooking.carId, bookingId, newRidingInCarServiceResponse)
        .transact(conn).unsafeToFuture()
    } yield newRideInOurService
  }

  def endRide(rideId: UUID, userId: Long): Future[Ride] = {
    for {
      ride <- getActiveRide(rideId, userId).transact(conn).unsafeToFuture()
      carServiceEndRideRequest <- carHttpService.endRide(ride.carRideId)
      updatedRide <- rideRepository
        .finishRide(rideId, 20, carServiceEndRideRequest.finalCoords.getOrElse(("20.202020, 30.303030)")))
        .transact(conn)
        .unsafeToFuture()
    } yield updatedRide
  }

}
