package ru.tinkoff.carsharing.userservice.services

import akka.actor.ActorSystem
import ru.tinkoff.carsharing.userservice.db.exceptions.{BookingAlreadyClosedException, ModelNotFoundException, RideAlreadyEndedException}
import ru.tinkoff.carsharing.userservice.db.repositories.{BookingRepository, RideRepository}
import ru.tinkoff.carsharing.userservice.http.services.CarHttpService
import doobie.implicits._

import java.util.UUID

class CarService(rideRepository: RideRepository, carHttpService: CarHttpService, rideService: RideService) {
  import ru.tinkoff.carsharing.userservice.db.configs.DoobieConfig.conn
  implicit val as = ActorSystem()
  import as.dispatcher

  def start(rideId: UUID, userId: Long) =
    for {
      ride <- rideService.getActiveRide(rideId, userId).transact(conn).unsafeToFuture()
      carRide <- carHttpService.startCar(ride.carId)
    } yield carRide

  def end(rideId: UUID, userId: Long) =
    for {
      ride <- rideService.getActiveRide(rideId, userId).transact(conn).unsafeToFuture()
      carRide <- carHttpService.endCar(ride.carId)
    } yield carRide


}
