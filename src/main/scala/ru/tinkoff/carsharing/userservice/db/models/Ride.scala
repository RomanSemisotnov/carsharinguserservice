package ru.tinkoff.carsharing.userservice.db.models

import java.time.LocalDateTime
import java.util.UUID

case class Ride(
                 id: UUID,
                 carId: Long,
                 carRideId: UUID,
                 userId: Long,
                 bookingId: Option[UUID],
                 started: LocalDateTime,
                 ended: Option[LocalDateTime],
                 startPoint: String,
                 endPoint: Option[String],
                 carName: Option[String],
                 price: Option[Double]
               )
