package ru.tinkoff.carsharing.userservice.db.models

import java.util.UUID

case class Booking(
                    id: UUID,
                    userId: Long,
                    carId: Long,
                    started: String,
                    ended: Option[String],
                    price: Option[Double]
                  )
