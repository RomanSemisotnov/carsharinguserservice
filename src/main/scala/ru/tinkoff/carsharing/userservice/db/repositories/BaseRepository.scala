package ru.tinkoff.carsharing.userservice.db.repositories

import doobie.implicits._

trait BaseRepository {

  def getLastVal() =
    sql"select lastval()".query[Long].unique

}
