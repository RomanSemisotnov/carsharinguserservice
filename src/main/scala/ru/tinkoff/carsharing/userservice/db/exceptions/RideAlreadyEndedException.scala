package ru.tinkoff.carsharing.userservice.db.exceptions

import java.util.UUID

case class RideAlreadyEndedException(rideId: UUID)
  extends Exception(s"ride with id: $rideId already ended")
