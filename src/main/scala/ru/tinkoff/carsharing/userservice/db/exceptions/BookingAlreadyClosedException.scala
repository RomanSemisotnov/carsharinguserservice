package ru.tinkoff.carsharing.userservice.db.exceptions

import java.util.UUID

case class BookingAlreadyClosedException(id: UUID) extends Exception(s"booking with id: $id already closed")
