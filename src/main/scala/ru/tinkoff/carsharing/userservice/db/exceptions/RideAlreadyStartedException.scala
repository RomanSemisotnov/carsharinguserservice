package ru.tinkoff.carsharing.userservice.db.exceptions


case class RideAlreadyStartedException(foundedBy: String)
  extends Exception(s"Ride $foundedBy already started")
