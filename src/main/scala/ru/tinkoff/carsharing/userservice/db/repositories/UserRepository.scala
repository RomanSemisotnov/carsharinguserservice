package ru.tinkoff.carsharing.userservice.db.repositories

import doobie.ConnectionIO
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.models.User

class UserRepository extends BaseRepository {

  def findById(id: Long): ConnectionIO[Option[User]] =
    sql"select id, login, password from users where id = $id"
      .query[User]
      .option

  def findByLogin(login: String): ConnectionIO[Option[User]] =
    sql"select id, login, password from users where login = $login"
      .query[User]
      .option

  def insert(login: String, pass: String): ConnectionIO[Int] =
    sql"insert into users (login, password) values ($login, $pass)"
      .update
      .run

}
