package ru.tinkoff.carsharing.userservice.db.models

case class User(
                 id: Long,
                 login: String,
                 password: String
               )
