package ru.tinkoff.carsharing.userservice.db.repositories

import doobie.ConnectionIO
import doobie._
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.models.{Booking, Ride}
import doobie.postgres._
import doobie.postgres.implicits._
import ru.tinkoff.carsharing.userservice.http.models.StartCarRideResponse

import java.time.LocalDateTime
import java.util.UUID

class BookingRepository extends BaseRepository {

  def findById(bookingId: UUID): ConnectionIO[Option[Booking]] =
    sql"select id, user_id, car_id, started, ended, price from bookings where id = $bookingId::uuid"
      .query[Booking]
      .option

  def findByUserIdAndBookingId(userId: Long, bookingId: UUID) =
    sql"select id, user_id, car_id, started, ended, price from bookings where id = $bookingId and user_id = $userId"
      .query[Booking]
      .option

  def makeBooking(userId: Long, carId: Long) =
    sql"insert into bookings (id, user_id, car_id, started, ended, price) values (${UUID.randomUUID}::uuid, $userId, $carId, now(), null, null)"
      .update
      .withUniqueGeneratedKeys[Booking]("id", "user_id", "car_id", "started", "ended", "price")

  def closeBooking(bookingId: UUID, price: Double) =
    sql"UPDATE bookings SET ended=now(), price = $price WHERE id=$bookingId"
      .update
      .withUniqueGeneratedKeys[Booking]("id", "user_id", "car_id", "started", "ended", "price")

  def checkCarAvailable(carId: Long) =
    (fr"select (SELECT count(*) FROM rides r where r.car_id = $carId and r.ended is null) + " ++
      fr"(select count(*) from bookings b where b.car_id = $carId and b.ended is null) ")
      .query[Long]
      .unique
      .map(_ == 0)

  def checkNotClosedBookingExist(userId: Long, bookingId: UUID): ConnectionIO[Option[Booking]] =
    sql"select id, user_id, car_id, started, ended, price from bookings where id = $bookingId and user_id = $userId and ended is null"
      .query[Booking]
      .option


}
