package ru.tinkoff.carsharing.userservice.db.exceptions

case class CarAlreadyUsedException(id: Long) extends Exception(s"Car with id: $id already use")
