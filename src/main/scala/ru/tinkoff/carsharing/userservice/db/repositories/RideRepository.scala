package ru.tinkoff.carsharing.userservice.db.repositories

import doobie.ConnectionIO
import doobie._
import doobie.implicits._
import ru.tinkoff.carsharing.userservice.db.models.{Booking, Ride}
import doobie.postgres._
import doobie.postgres.implicits._
import ru.tinkoff.carsharing.userservice.http.models.StartCarRideResponse

import java.time.LocalDateTime
import java.util.UUID
import scala.annotation.nowarn

class RideRepository extends BaseRepository {

  def findByRideIdAndUserId(rideId: UUID, userId: Long): ConnectionIO[Option[Ride]] =
    sql"select id, car_id, car_ride_id, user_id, booking_id, started, ended, start_point, end_point, car_name, price from rides where id=$rideId and user_id = $userId"
      .query[Ride]
      .option

  def findByByBookingAndUserIdNotEnded(bookingId: UUID, userId: Long) =
    sql"select id, car_id, car_ride_id, user_id, booking_id, started, ended, start_point, end_point, car_name, price from rides where booking_id=$bookingId and user_id = $userId and ended is null"
      .query[Ride]
      .option

  def startRide(userId: Long, carId: Long, bookingId: UUID, carData: StartCarRideResponse): ConnectionIO[Ride] =
    (fr"INSERT INTO public.rides" ++
      fr"(id, car_id, car_ride_id, user_id, booking_id, started, ended, start_point, end_point, car_name, price)" ++
      fr"VALUES(${UUID.randomUUID}, $carId, ${carData.rideId}, $userId, $bookingId, now(), null, ${carData.currentCoords}, null, ${carData.carName}, null);")
      .update
      .withUniqueGeneratedKeys[Ride](
        "id", "car_id", "car_ride_id", "user_id", "booking_id",
        "started", "ended", "start_point", "end_point", "car_name", "price")

  def finishRide(rideId: UUID, price: Double, finishCoord: String) =
    sql"UPDATE rides SET ended=now(), price = $price, end_point = $finishCoord WHERE id = $rideId"
      .update
      .withUniqueGeneratedKeys[Ride](
        "id", "car_id", "car_ride_id", "user_id", "booking_id",
        "started", "ended", "start_point", "end_point", "car_name", "price")

  def getRides(userId: Long) =
    (sql"select id, car_id, car_ride_id, user_id, booking_id, started, ended, start_point, end_point, car_name, price from rides where user_id = $userId order by ended desc")
      .query[Ride]
      .to[List]

}
