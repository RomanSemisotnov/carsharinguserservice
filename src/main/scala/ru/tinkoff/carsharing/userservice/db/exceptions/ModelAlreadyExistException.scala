package ru.tinkoff.carsharing.userservice.db.exceptions

case class ModelAlreadyExistException[T](model: String, primaryKey: AnyRef)
  extends Exception(s"$model already exist with primaryKey: ${primaryKey.toString}")