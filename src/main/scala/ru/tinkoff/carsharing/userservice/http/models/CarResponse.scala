package ru.tinkoff.carsharing.userservice.http.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class CarResponse(
                id: Long,
                name: String,
                isAvailable: Boolean,
                isStarted: Boolean,
                isLocked: Boolean,
                gasolineState: Double,
                coords: String
              )

case object CarResponse extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val carFormat = jsonFormat7(CarResponse.apply)
}
