package ru.tinkoff.carsharing.userservice.http.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.Unmarshal
import ru.tinkoff.carsharing.userservice.http.models.AvailableCarResponse

import scala.concurrent.Future

trait BaseHttpService {
  private implicit val as = ActorSystem()
  import as.dispatcher

  private val carServiceIP = "http://localhost:8081/api/v1"

  private val retryCount = 20

  private def retry(f: => Future[HttpResponse]): Future[HttpResponse] = {
    def retry(f: => Future[HttpResponse], retries: Int): Future[HttpResponse] = {
      f.flatMap {
        case r if r.status.intValue() == 200 || r.status.intValue() == 201 => Future.successful(r)
        case r if r.status.intValue() >= 500 => retry(f, retries - 1)
        case r => Future.failed(new Exception(r.entity.toString))
      }
    }
    retry(f, retryCount)
  }

  protected def httpGet(path: String): Future[HttpResponse] =
    retry(Http().singleRequest(HttpRequest(uri = carServiceIP + path)))

  protected def httpPost(path: String): Future[HttpResponse] =
    retry(Http().singleRequest(HttpRequest(uri = carServiceIP + path, method = HttpMethods.POST)))

  protected def httpPost[T](path: String, body: String): Future[HttpResponse] =
    retry(Http().singleRequest(HttpRequest(
      uri = carServiceIP + path,
      method = HttpMethods.POST,
      entity = HttpEntity(ContentTypes.`application/json`, body.getBytes)
    )))

}
