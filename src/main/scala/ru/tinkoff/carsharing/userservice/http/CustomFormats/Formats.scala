package ru.tinkoff.carsharing.userservice.http.CustomFormats

import doobie.Meta
import spray.json.{JsString, JsValue, RootJsonFormat, deserializationError}

import java.time.LocalDateTime
import java.util.UUID
import java.time.format.DateTimeFormatter

class UuidJsonFormat extends RootJsonFormat[UUID] {
  def write(x: UUID) = JsString(x.toString) //Never execute this line
  def read(value: JsValue) = value match {
    case JsString(x) => UUID.fromString(x)
    case x           => deserializationError("Expected UUID as JsString, but got " + x)
  }
}

object Formats {
  implicit val uuid = new UuidJsonFormat

  implicit val localDateTimeMeta: Meta[LocalDateTime] =
    Meta[String].imap[LocalDateTime] { dateTime =>
      val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
      LocalDateTime.parse(dateTime, formatter)
    }(_.toString)
}
