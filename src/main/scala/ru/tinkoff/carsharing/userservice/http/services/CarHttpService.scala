package ru.tinkoff.carsharing.userservice.http.services

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.unmarshalling.Unmarshal
import io.circe.generic.auto._
import io.circe.syntax._
import ru.tinkoff.carsharing.userservice.http.models.{AvailableCarResponse, CarResponse, EndCarRideResponse, StartCarRideResponse}

import scala.concurrent.Future
import ru.tinkoff.carsharing.userservice.http.models.StartCarRideResponse.startCarRideResponse

import java.util.UUID


class CarHttpService extends BaseHttpService {
  implicit val as = ActorSystem()
  import as.dispatcher

  def getAvailable(): Future[AvailableCarResponse] =
    httpGet("/car/available")
      .flatMap { resp =>
        Unmarshal(resp.entity).to[AvailableCarResponse]
      }

  case class StartRideRequest(carId: Long)
  def startRide(carId: Long) =
    httpPost("/car/ride/start", StartRideRequest(carId).asJson.toString)
      .flatMap { resp =>
        Unmarshal(resp.entity).to[StartCarRideResponse]
      }

  case class StartCarRequest(carId: Long)
  def startCar(carId: Long) =
    httpPost("/car/start", StartCarRequest(carId).asJson.toString)
      .flatMap { resp =>
        Unmarshal(resp.entity).to[CarResponse]
      }

  case class EndCarRequest(carId: Long)
  def endCar(carId: Long) =
    httpPost("/car/end", EndCarRequest(carId).asJson.toString)
      .flatMap { resp =>
        Unmarshal(resp.entity).to[CarResponse]
      }

  case class EndRideRequest(carRideId: UUID)
  def endRide(carId: UUID) =
    httpPost("/car/ride/end", EndRideRequest(carId).asJson.toString)
      .flatMap { resp =>
        Unmarshal(resp.entity).to[EndCarRideResponse]
      }

}
