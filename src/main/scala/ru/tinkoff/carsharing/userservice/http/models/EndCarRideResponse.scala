package ru.tinkoff.carsharing.userservice.http.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class EndCarRideResponse(
                               finalCoords: Option[String],
                               carId: Long
                             )

case object EndCarRideResponse extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val startCarRideResponse =
    jsonFormat2(EndCarRideResponse.apply)
}