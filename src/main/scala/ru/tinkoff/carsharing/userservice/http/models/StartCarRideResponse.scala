package ru.tinkoff.carsharing.userservice.http.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import java.util.UUID
import ru.tinkoff.carsharing.userservice.http.CustomFormats.Formats.uuid

case class StartCarRideResponse(
                                 carName: String,
                                 rideId: UUID,
                                 currentCoords: String
                               )

case object StartCarRideResponse extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val startCarRideResponse =
    jsonFormat3(StartCarRideResponse.apply)
}
