package ru.tinkoff.carsharing.userservice.http.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class AvailableCarResponse(availableCars: List[CarResponse])

case object AvailableCarResponse extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val availableCarResponseFormat =
    jsonFormat1(AvailableCarResponse.apply)
}

