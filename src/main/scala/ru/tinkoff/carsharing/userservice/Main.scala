package ru.tinkoff.carsharing.userservice

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import ru.tinkoff.carsharing.userservice.api.endpoints.{BookingEndpoint, CarEndpoint, OrderEndpoint, RideEndpoint, UserEndpoint}
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter

object Main {

  def main(args: Array[String]): Unit = {
    implicit val as: ActorSystem = ActorSystem()
    import as.dispatcher

    val userEndpoint = new UserEndpoint
    val carEndpoint = new CarEndpoint
    val bookingEndpoint = new BookingEndpoint
    val rideEndpoint = new RideEndpoint
    val orderEndpoint = new OrderEndpoint

    val allEndpoints =
      userEndpoint.all ::: carEndpoint.all :::
        bookingEndpoint.all ::: rideEndpoint.all ::: orderEndpoint.all

    val routes: Route = AkkaHttpServerInterpreter.toRoute(allEndpoints)
    Http().newServerAt("localhost", 8080)
      .bind(routes)
      .foreach(b => println(s"server started at ${b.localAddress}"))
  }

}
