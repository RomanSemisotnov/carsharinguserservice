CREATE TABLE users
(
    Id SERIAL PRIMARY KEY,
    login varchar(255) UNIQUE NOT NULL,
    password varchar(255) NOT NULL
);
CREATE TABLE sessions
(
    Id uuid PRIMARY KEY,
    user_id int REFERENCES users (id) UNIQUE NOT NULL,
    val varchar(255) NOT NULL
);
CREATE TABLE bookings
(
    id      uuid PRIMARY KEY,
    user_id int REFERENCES users (id) NOT NULL,
    car_id  int                       NOT NULL,
    started timestamp                 NOT NULL,
    ended   timestamp,
    price   decimal
);
CREATE TABLE rides
(
    id uuid PRIMARY KEY,
    car_id int NOT NULL,
    car_ride_id uuid NOT NULL,
    user_id int REFERENCES users (id) NOT NULL,
    booking_id uuid REFERENCES bookings (id),
    started timestamp NOT NULL,
    ended timestamp,
    start_point varchar(255) NOT NULL,
    end_point varchar(255),
    car_name varchar(255) NOT NULL,
    price decimal
);

INSERT INTO bookings
(id, car_id, started, ended, user_id)
VALUES (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 1, now(), now(), 1),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 1, now(), now(), 1),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 2, now(), now(), 1),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 3, now(), NULL, 1),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 4, now(), now(), 1);
-- 1,2 - свободна, 3, 4 - заняты
INSERT INTO rides
(id, car_ride_id, car_id, started, ended, user_id, start_point, car_name)
VALUES (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 1,now(), now(), 1, '37.123123123, 37.423423', 'bmw'),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 2,now(), now(), 1, '37.123123123, 37.423423', 'bmw'),
       (uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), uuid_in(md5(random()::text || clock_timestamp()::text)::cstring), 4,now(), NULL, 1, '37.123123123, 37.423423', 'bmw');
